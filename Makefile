
# Where this file resides

root_dir := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

# When version changes, increment this number

VERSION := 1.0.0

# Determine where we are going to install things

prefix := $(HOME)/.local
exec_prefix := $(prefix)
includedir := $(prefix)/include
libdir := $(exec_prefix)/lib

# According to the GNU Make documentation: "Every Makefile should define the
# variable INSTALL, which is the basic command for installing a file into the
# system.  Every Makefile should also define the variables INSTALL_PROGRAM and
# INSTALL_DATA."

INSTALL := install -D
INSTALL_PROGRAM := $(INSTALL)
INSTALL_DATA := $(INSTALL) -m 644

# Overridable flags

CXXFLAGS := -O3
CFLAGS := -O3

# Override these flags so they can't be re-overriden.

override CXXFLAGS += -std=c++20
override CXXFLAGS += -MMD
override CXXFLAGS += -fPIC
override CXXFLAGS += -fvisibility=hidden
override CXXFLAGS += -Wall
override CXXFLAGS += -Werror
override CXXFLAGS += -Wfatal-errors

override CFLAGS += -std=gnu2x
override CFLAGS += -MMD
override CFLAGS += -fPIC
override CFLAGS += -fvisibility=hidden
override CFLAGS += -Wall
override CFLAGS += -Werror
override CFLAGS += -Wfatal-errors

override CPPFLAGS += -I$(root_dir)

# Compilation takes place in...

vpath %.c $(root_dir)mdea $(root_dir)tests
vpath %.cc $(root_dir)mdea $(root_dir)tests

# All tests to be compiled and executed

objs += tree.o
objs += emit.o
objs += scan.o

tests += tests/menu.json
tests += tests/widget.json
tests += tests/glossary.json
tests += tests/menu2.json
tests += tests/princess-and-undead.json

.PHONY: all
all: libmdea.a libmdea.so

.PHONY: check
check: test_scan_c test_scan_cc test_emit_c test_emit_cc

.PHONY: clean
clean:
	$(RM) $(objs)
	$(RM) $(objs:.o=.d)
	$(RM) libmdea.a
	$(RM) libmdea.so
	$(RM) test_emit_c
	$(RM) test_emit_cc
	$(RM) test_scan_c
	$(RM) test_scan_cc

.PHONY: install
install:
	$(INSTALL_DATA) $(root_dir)mdea/tree.h $(DESTDIR)$(includedir)/mdea/tree.h
	$(INSTALL_DATA) $(root_dir)mdea/scan.h $(DESTDIR)$(includedir)/mdea/scan.h
	$(INSTALL_DATA) $(root_dir)mdea/emit.h $(DESTDIR)$(includedir)/mdea/emit.h
	$(INSTALL_DATA) libmdea.a $(DESTDIR)$(libdir)
	$(INSTALL_DATA) libmdea.so $(DESTDIR)$(libdir)

.PHONY: uninstall
uninstall:
	-$(RM) $(DESTDIR)$(includedir)/mdea/tree.h
	-$(RM) $(DESTDIR)$(includedir)/mdea/scan.h
	-$(RM) $(DESTDIR)$(includedir)/mdea/emit.h
	-$(RM) $(DESTDIR)$(libdir)/libmdea.a
	-$(RM) $(DESTDIR)$(libdir)/libmdea.so

libmdea.a: $(objs)
	$(AR) rc $@ $^

libmdea.so: $(objs)
	$(CC) -shared -o $@ $^

test_scan_c: test_scan.c libmdea.a
	$(CC) $(CPPFLAGS) $(CFLAGS) $(LDFLAGS) -o $@ $^ $(LDLIBS)

test_scan_cc: test_scan.cc libmdea.a
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $(LDFLAGS) -o $@ $^ $(LDLIBS)

test_emit_c: test_emit.c libmdea.a
	$(CC) $(CPPFLAGS) $(CFLAGS) $(LDFLAGS) -o $@ $^ $(LDLIBS)

test_emit_cc: test_emit.cc libmdea.a
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $(LDFLAGS) -o $@ $^ $(LDLIBS)

-include $(shell find -name "*.d")
