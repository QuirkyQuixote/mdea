MDEA - A JSON Parser
====================

A simple library to serialize and deserialize C and C++ data to and from JSON.

Provides
--------

Both the C and C++ versions of the library requires linking with either
`libmdea.a` or `libmdea.so`.

* Include [`mdea/scan.h`](mdea/scan.h) ([C](doc/scan_c.md), [C++](doc/scan_cc.md)) for the scanner.
* Include [`mdea/emit.h`](mdea/emit.h) ([C](doc/emit_c.md), [C++](doc/emit_cc.md)) for the emitter.
* Include [`mdea/mdea.h`](mdea/mdea.h) for both.

Install
-------

Run `make install` to install:

* headers are copied to `$(PREFIX)$(includedir)/mdea/`
  * (by default `$(HOME)/.local/include/mdea/`)
* the library is copied to `$(PREFIX)$(libdir)`
  * (by default `$(HOME)/.local/lib`)

You can remove the installed files with `make uninstall`.


Usage
-----

The C++ API is designed so the code for emitting and scanning can be the same:

```cpp
struct Data {
        bool boolean{0};
        double number{0};
        const char *string{""};
        double array[3] = {0};
        double object[3] = {0};
};

bool operator&(mdea::stream auto& s, mdea::maybe_const<Data> auto& d)
{
        if (auto _ = s.object()) {
                s.name("null") % std::nullopt;
                s.name("boolean") % d.boolean;
                s.name("number") % d.number;
                s.name("string") % d.string;
                if (auto _ = s.name("array").array()) {
                         s.next() % d.array[0];
                         s.next() % d.array[1];
                         s.next() % d.array[2];
                }
                if (auto _ = s.name("object").object()) {
                         s.name("foo") % d.object[0];
                         s.name("bar") % d.object[1];
                         s.name("baz") % d.object[2];
                }
                return true;
        }
        return false;
}
```

This allows to use the operator to both read and write:

```cpp
Data d;
mdea::File_scanner s{path};
s % d;
```

```cpp
Data d;
mdea::File_emitter s{path};
s % d;
```
