// Include this for the full library

// Copyright (C) 2021-2024 L. Sanz <luis.sanz@gmail.com>

// This file is part of MDEA.

// MDEA is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// MDEA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with MDEA.  If not, see <https://www.gnu.org/licenses/>.

#ifndef mdea_mdea_h
#define mdea_mdea_h

#include "scan.h"
#include "emit.h"

#ifdef __cplusplus

namespace mdea {

template<class T> concept stream = scanner<T> || emitter<T>;

template<class T, class S> concept maybe_const = std::same_as<std::remove_const_t<T>, S>;

}; // namespace mdea

#endif // __cplusplus

#endif // _mdea_h
