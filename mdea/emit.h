// header for the MDEA emitter

// Copyright (C) 2021-2024 L. Sanz <luis.sanz@gmail.com>

// This file is part of MDEA.

// MDEA is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// MDEA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with MDEA.  If not, see <https://www.gnu.org/licenses/>.

#ifndef mdea_emit_h
#define mdea_emit_h

#include <stdio.h>
#include <inttypes.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

// Flags that modify the emitter behavior
// indent:              indent code by two spaces per level

enum mdea_emitter_flags {
        mdea_emitter_indent = 1 << 0,
};

// Internal state of the emitter;
// determines what the next element is supposed to be
// root:                the root of the tree
// array_first:         first element of an array
// array:               second to last element of an array
// object_first:        first field of an object
// object:              second to last element of an object

enum mdea_emitter_state {
        mdea_emitter_state_root,
        mdea_emitter_state_array_first,
        mdea_emitter_state_array,
        mdea_emitter_state_object_first,
        mdea_emitter_state_object,
};

// Error codes
// ok:                  nothing happened
// can not open:        emitter initialization failed
// expected key:        pushed something other than string when expecting a key
// expected value:      failed to push a value when one was expected
// stack underflow:     attempted to add items after the root was complete

enum mdea_emitter_error {
        mdea_emitter_ok,
        mdea_emitter_can_not_open,
        mdea_emitter_io_error,
        mdea_emitter_expected_key,
        mdea_emitter_expected_value,
        mdea_emitter_stack_underflow,
};

// Data for the actual emitter
//
// The emitter owns the file that it's handling; it's opened when the emitter
// is initialized and closed when it's deinitialized.
//
// The emitter keeps a stack of states that traces a line from the root element
// to the one currently being added at.

struct mdea_emitter {
        FILE *f;
        int flags;
        int error;
        enum mdea_emitter_state stack[64];
        enum mdea_emitter_state *stackp;
        const char *next;
};

// Construct emitter
//
// It can be created from a path, in which case the file will be opened for
// writing and data will be removed, or from a string using open_memstream()

struct mdea_emitter* mdea_emitter_init_file(struct mdea_emitter *d,
                const char *path, int flags);
struct mdea_emitter* mdea_emitter_init_mem(struct mdea_emitter *d,
                char **buf, size_t *len, int flags);

// Destroy emitter
void mdea_emitter_free(struct mdea_emitter* d);

// Return 1 if ready to receive data
int mdea_emitter_ready(const struct mdea_emitter *e);

// Navigate the data
//
// mdea_emit_array() and mdea_emit_object() will push a new array or object to
// the stack.
//
// mdea_emit_next() and mdea_emit_name() will prepare the emitter to write the
// next value of the element at the top of the stack; if an array, this is the
// next element, if an object, it is the element named `name`.
//
// mdea_emit_end() pops the top of the stack.

int mdea_emit_array(struct mdea_emitter* d);
int mdea_emit_object(struct mdea_emitter *d);
int mdea_emit_next(struct mdea_emitter *e);
int mdea_emit_name(struct mdea_emitter *e, const char *n);
int mdea_emit_end(struct mdea_emitter* d);

// Emit values
//
// mdea_emit_FOO() builds an atomic value from the given data at the position
// the cursor is pointing at.

int mdea_emit_null(struct mdea_emitter* d);
int mdea_emit_boolean(struct mdea_emitter* d, bool b);
int mdea_emit_number(struct mdea_emitter* d, const char *n);
int mdea_emit_i8(struct mdea_emitter* d, int8_t n);
int mdea_emit_i16(struct mdea_emitter* d, int16_t n);
int mdea_emit_i32(struct mdea_emitter* d, int32_t n);
int mdea_emit_i64(struct mdea_emitter* d, int64_t n);
int mdea_emit_u8(struct mdea_emitter* d, uint8_t n);
int mdea_emit_u16(struct mdea_emitter* d, uint16_t n);
int mdea_emit_u32(struct mdea_emitter* d, uint32_t n);
int mdea_emit_u64(struct mdea_emitter* d, uint64_t n);
int mdea_emit_float(struct mdea_emitter* d, float n);
int mdea_emit_double(struct mdea_emitter* d, double n);
int mdea_emit_string(struct mdea_emitter* d, const char *s);

// Return an error message for the given error code.
const char* mdea_emitter_error_message(int e);

#ifdef __cplusplus
}; // extern "C"
#endif // __cplusplus

#ifdef __cplusplus

#include <string>
#include <string_view>
#include <filesystem>
#include <optional>

namespace mdea {

// C++ wrapper for an emitter
// 
// Emitter is a base class for other implementations; see File_emitter and
// string emitter further below for details.
//
// There are two variants of the functions that emit elements: untagged and
// tagged; the tagged ones take two arguments, the firsh of which is an string,
// and is used to automate the construction of object fields.
//
// The array() and object() functions return lock objects that are used to
// keep track of the stack using RAII; when the lock object is destroyed, the
// array or object is removed from the stack.

template<class Derived> class Emitter {
 private:
        constexpr mdea_emitter *emit()
        { return &static_cast<Derived*>(this)->emit; };
        constexpr const mdea_emitter *emit() const
        { return &static_cast<const Derived*>(this)->emit; };

 protected:
        Emitter() = default;

 public:
        class Lock {
                friend class Emitter;
         private:
                mdea_emitter* emit;
         protected:
                Lock(mdea_emitter *emit) : emit{emit} {}
         public:
                Lock(const Lock&) = delete;
                Lock(Lock&&) = delete;
                ~Lock() { mdea_emit_end(emit); }
                operator bool() const { return !emit->error; }
        };

        Emitter(const Emitter&) = delete;
        Emitter(Emitter&&) = delete;

        operator bool() const { return mdea_emitter_ready(emit()); }

        Derived& next()
        {
                mdea_emit_next(emit());
                return *static_cast<Derived*>(this);
        }

        Derived& name(const char *n)
        {
                mdea_emit_name(emit(), n);
                return *static_cast<Derived*>(this);
        }

        bool operator%(std::nullopt_t) { return mdea_emit_null(emit()); }
        bool operator%(bool b) { return mdea_emit_boolean(emit(), b); }
        bool operator%(int8_t n) { return mdea_emit_i8(emit(), n); }
        bool operator%(int16_t n) { return mdea_emit_i16(emit(), n); }
        bool operator%(int32_t n) { return mdea_emit_i32(emit(), n); }
        bool operator%(int64_t n) { return mdea_emit_i64(emit(), n); }
        bool operator%(uint8_t n) { return mdea_emit_u8(emit(), n); }
        bool operator%(uint16_t n) { return mdea_emit_u16(emit(), n); }
        bool operator%(uint32_t n) { return mdea_emit_u32(emit(), n); }
        bool operator%(uint64_t n) { return mdea_emit_u64(emit(), n); }
        bool operator%(float n) { return mdea_emit_float(emit(), n); }
        bool operator%(double n) { return mdea_emit_double(emit(), n); }

        template<class E>
        requires std::is_enum_v<E>
        bool operator%(E n)
        { return (*this) % static_cast<std::underlying_type_t<E>>(n); }

        bool operator%(const char *s) { return mdea_emit_string(emit(), s); }
        bool operator%(const std::string& s) { return mdea_emit_string(emit(), s.data()); }
        bool operator%(const std::string_view& s) { return mdea_emit_string(emit(), s.data()); }

        auto array()
        {
                mdea_emit_array(emit());
                return Lock{emit()};
        }

        auto object()
        {
                mdea_emit_object(emit());
                return Lock{emit()};
        }

        const char* error()
        { return mdea_emitter_error_message(emit()->error); }
};

// Emitter specialization that outputs JSON to a file
class File_emitter : public Emitter<File_emitter> {
        friend class Emitter<File_emitter>;
 protected:
        mdea_emitter emit{nullptr};
 public:
        File_emitter(const char* path, int flags = mdea_emitter_indent)
        { mdea_emitter_init_file(&emit, path, flags); }

        File_emitter(const std::string& path, int flags = mdea_emitter_indent)
                : File_emitter(path.data(), flags) {}

        File_emitter(const std::filesystem::path& path, int flags = mdea_emitter_indent)
                : File_emitter(path.string().data(), flags) {}

        ~File_emitter()
        {
                mdea_emitter_free(&emit);
        }
};

// Emitter specialization that outputs JSON to a string
class String_emitter : public Emitter<String_emitter> {
        friend class Emitter<String_emitter>;
 private:
        char *buf{nullptr};
        size_t len{0};
 protected:
        mdea_emitter emit{nullptr};
 public:
        String_emitter(int flags = mdea_emitter_indent)
        { mdea_emitter_init_mem(&emit, &buf, &len, flags); }

        ~String_emitter()
        {
                mdea_emitter_free(&emit);
                free(buf);
        }

        std::string_view string()
        {
                fflush(emit.f);
                return std::string_view(buf, buf + len);
        }
};

template<class T> struct is_emitter : public std::integral_constant<bool, false> {};
template<> struct is_emitter<String_emitter> : public std::integral_constant<bool, true> {};
template<> struct is_emitter<File_emitter> : public std::integral_constant<bool, true> {};
template<class T> concept emitter = is_emitter<T>::value;

}; // namespace mdea

#endif // __cplusplus

#endif // mdea_emit_h

