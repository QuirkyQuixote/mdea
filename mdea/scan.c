
#include "scan.h"

struct mdea_scanner *mdea_scanner_init_string(struct mdea_scanner *s, const char *p, size_t n)
{
        s->stackp = s->stack;
        if (mdea_tree_init_string(&s->tree, p, n) == NULL) {
                s->stackp->type = -1;
                return NULL;
        }
        s->stackp->type = 0;
        s->val = s->tree.root;
        return s;
}

struct mdea_scanner *mdea_scanner_init_file(struct mdea_scanner *s, int fd)
{
        s->stackp = s->stack;
        if (mdea_tree_init_file(&s->tree, fd) == NULL) {
                s->stackp->type = -1;
                return NULL;
        }
        s->stackp->type = 0;
        s->val = s->tree.root;
        return s;
}

struct mdea_scanner *mdea_scanner_init_path(struct mdea_scanner *s, const char *path)
{
        s->stackp = s->stack;
        if (mdea_tree_init_path(&s->tree, path) == NULL) {
                s->stackp->type = -1;
                return NULL;
        }
        s->stackp->type = 0;
        s->val = s->tree.root;
        return s;
}

void mdea_scanner_free(struct mdea_scanner *s)
{
        mdea_tree_free(&s->tree);
}

int mdea_scanner_ready(const struct mdea_scanner *s)
{
        return s->val != 0;
}

int mdea_scan_next(struct mdea_scanner *s)
{
        if (s->stackp->type != mdea_array)
                return 0;
        s->val = mdea_tree_array_at(&s->tree,
                        s->stackp->arr.arr,
                        s->stackp->arr.next++);
        return !!s->val;
}

int mdea_scan_name(struct mdea_scanner *s, const char *n)
{
        if (s->stackp->type != mdea_object)
                return 0;
        const struct mdea_pair *i;
        if ((i = mdea_tree_object_at(&s->tree, s->stackp->obj.obj, n)) == NULL)
                return -1;
        s->stackp->obj.next = i->k;
        s->val = i->v;
        return 0;
}

int mdea_scan_null(struct mdea_scanner *s)
{
        if (mdea_tree_type(s->val) != mdea_null) return 0;
        s->val = 0;
        return 1;
}

int mdea_scan_boolean(struct mdea_scanner *s, bool *r)
{
        if (mdea_tree_type(s->val) != mdea_boolean) return 0;
        *r = mdea_tree_bool(&s->tree, s->val);
        s->val = 0;
        return 1;
}

int mdea_scan_number(struct mdea_scanner *s, const char **r)
{
        if (mdea_tree_type(s->val) != mdea_number) return 0;
        *r = mdea_tree_number(&s->tree, s->val);
        s->val = 0;
        return 1;
}

int mdea_scan_i8(struct mdea_scanner *s, int8_t *r)
{
        if (mdea_tree_type(s->val) != mdea_number) return 0;
        sscanf(mdea_tree_number(&s->tree, s->val), "%" SCNd8, r);
        s->val = 0;
        return 1;
}

int mdea_scan_i16(struct mdea_scanner *s, int16_t *r)
{
        if (mdea_tree_type(s->val) != mdea_number) return 0;
        sscanf(mdea_tree_number(&s->tree, s->val), "%" SCNd16, r);
        s->val = 0;
        return 1;
}

int mdea_scan_i32(struct mdea_scanner *s, int32_t *r)
{
        if (mdea_tree_type(s->val) != mdea_number) return 0;
        sscanf(mdea_tree_number(&s->tree, s->val), "%" SCNd32, r);
        s->val = 0;
        return 1;
}

int mdea_scan_i64(struct mdea_scanner *s, int64_t *r)
{
        if (mdea_tree_type(s->val) != mdea_number) return 0;
        sscanf(mdea_tree_number(&s->tree, s->val), "%" SCNd64, r);
        s->val = 0;
        return 1;
}

int mdea_scan_u8(struct mdea_scanner *s, uint8_t *r)
{
        if (mdea_tree_type(s->val) != mdea_number) return 0;
        sscanf(mdea_tree_number(&s->tree, s->val), "%" SCNu8, r);
        s->val = 0;
        return 1;
}

int mdea_scan_u16(struct mdea_scanner *s, uint16_t *r)
{
        if (mdea_tree_type(s->val) != mdea_number) return 0;
        sscanf(mdea_tree_number(&s->tree, s->val), "%" SCNu16, r);
        s->val = 0;
        return 1;
}

int mdea_scan_u32(struct mdea_scanner *s, uint32_t *r)
{
        if (mdea_tree_type(s->val) != mdea_number) return 0;
        sscanf(mdea_tree_number(&s->tree, s->val), "%" SCNu32, r);
        s->val = 0;
        return 1;
}

int mdea_scan_u64(struct mdea_scanner *s, uint64_t *r)
{
        if (mdea_tree_type(s->val) != mdea_number) return 0;
        sscanf(mdea_tree_number(&s->tree, s->val), "%" SCNu64, r);
        s->val = 0;
        return 1;
}

int mdea_scan_float(struct mdea_scanner *s, float *r)
{
        if (mdea_tree_type(s->val) != mdea_number) return 0;
        *r = strtof(mdea_tree_number(&s->tree, s->val), NULL);
        s->val = 0;
        return 1;
}

int mdea_scan_double(struct mdea_scanner *s, double *r)
{
        if (mdea_tree_type(s->val) != mdea_number) return 0;
        *r = strtod(mdea_tree_number(&s->tree, s->val), NULL);
        s->val = 0;
        return 1;
}

int mdea_scan_string(struct mdea_scanner *s, const char **r)
{
        if (mdea_tree_type(s->val) != mdea_string) return 0;
        *r = mdea_tree_string(&s->tree, s->val);
        s->val = 0;
        return 1;
}

int mdea_scan_array(struct mdea_scanner *s)
{
        if (mdea_tree_type(s->val) != mdea_array) return 0;
        s->stackp++;
        s->stackp->type = mdea_array;
        s->stackp->arr.arr = mdea_tree_array(&s->tree, s->val);
        s->stackp->arr.next = 0;
        s->val = 0;
        return 1;
}

int mdea_scan_object(struct mdea_scanner *s)
{
        if (mdea_tree_type(s->val) != mdea_object) return 0;
        s->stackp++;
        s->stackp->type = mdea_object;
        s->stackp->obj.obj = mdea_tree_object(&s->tree, s->val);
        s->val = 0;
        return 1;
}

int mdea_scan_end(struct mdea_scanner *s)
{
        if (s->stackp == s->stack)
                return 0;
        s->stackp--;
        s->val = 0;
        return 1;
}

int mdea_scan_error(struct mdea_scanner *s, const char *message)
{
        fflush(stdout);
        for (union mdea_scanner_stack *i = s->stack; i != s->stackp; ++i)
                switch (i->type) {
                 case mdea_array:
                        fprintf(stderr, "[%d]", i->arr.next - 1);
                        break;
                 case mdea_object:
                        fprintf(stderr, ".%s", mdea_tree_string(&s->tree, i->obj.next));
                        break;
                 default:
                        break;
                }
        fprintf(stderr, ": %s\n", message);
        return 0;
}

