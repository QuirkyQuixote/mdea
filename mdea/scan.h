
#ifndef mdea_scan_h
#define mdea_scan_h

#include <inttypes.h>

#include "tree.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

struct mdea_scanner_stack_array {
        int8_t type;
        const struct mdea_array *arr;
        int32_t next;
};

struct mdea_scanner_stack_object {
        int8_t type;
        const struct mdea_object *obj;
        int32_t next;
};

union mdea_scanner_stack {
        int8_t type;
        struct mdea_scanner_stack_array arr;
        struct mdea_scanner_stack_object obj;
};

struct mdea_scanner {
        struct mdea_tree tree;
        union mdea_scanner_stack stack[64];
        union mdea_scanner_stack *stackp;
        int32_t val;
};

struct mdea_scanner *mdea_scanner_init_string(struct mdea_scanner *s, const char *p, size_t n);
struct mdea_scanner *mdea_scanner_init_file(struct mdea_scanner *s, int fd);
struct mdea_scanner *mdea_scanner_init_path(struct mdea_scanner *s, const char *path);

void mdea_scanner_free(struct mdea_scanner *s);

int mdea_scanner_ready(const struct mdea_scanner *s);

int mdea_scan_array(struct mdea_scanner *s);
int mdea_scan_object(struct mdea_scanner *s);
int mdea_scan_next(struct mdea_scanner *s);
int mdea_scan_name(struct mdea_scanner *s, const char *n);
int mdea_scan_end(struct mdea_scanner *s);

int mdea_scan_null(struct mdea_scanner *s);
int mdea_scan_boolean(struct mdea_scanner *s, bool *r);
int mdea_scan_number(struct mdea_scanner *s, const char **r);
int mdea_scan_i8(struct mdea_scanner *s, int8_t *r);
int mdea_scan_i16(struct mdea_scanner *s, int16_t *r);
int mdea_scan_i32(struct mdea_scanner *s, int32_t *r);
int mdea_scan_i64(struct mdea_scanner *s, int64_t *r);
int mdea_scan_u8(struct mdea_scanner *s, uint8_t *r);
int mdea_scan_u16(struct mdea_scanner *s, uint16_t *r);
int mdea_scan_u32(struct mdea_scanner *s, uint32_t *r);
int mdea_scan_u64(struct mdea_scanner *s, uint64_t *r);
int mdea_scan_float(struct mdea_scanner *s, float *r);
int mdea_scan_double(struct mdea_scanner *s, double *r);
int mdea_scan_string(struct mdea_scanner *s, const char **r);

int mdea_scan_error(struct mdea_scanner *s, const char *message);

#ifdef __cplusplus
}; // extern "C"
#endif // __cplusplus

#ifdef __cplusplus

#include <charconv>
#include <string>
#include <string_view>
#include <filesystem>
#include <optional>

namespace mdea {

template<class Derived> class Scanner {
 private:
        constexpr mdea_scanner* scan()
        { return &static_cast<Derived*>(this)->scan; }
        constexpr const mdea_scanner* scan() const
        { return &static_cast<const Derived*>(this)->scan; }

 protected:
        Scanner() = default;

 public:
        struct Lock {
                friend class Scanner;
         private:
                mdea_scanner *scan;
         protected:
                Lock(mdea_scanner *scan = nullptr) : scan{scan} {}
         public:
                Lock(const Lock&) = delete;
                Lock(Lock&&) = delete;
                ~Lock() { if (scan) mdea_scan_end(scan); }
                constexpr operator bool() const { return scan; }
        };

        Scanner(const Scanner&) = delete;
        Scanner(Scanner&&) = delete;

        constexpr operator bool() const { return scan()->val; }

        Derived& next()
        {
                mdea_scan_next(scan());
                return *static_cast<Derived*>(this);
        }

        Derived& name(const char* n)
        {
                mdea_scan_name(scan(), n);
                return *static_cast<Derived*>(this);
        }

        bool operator%(const std::nullopt_t&) { return mdea_scan_null(scan()); }
        bool operator%(bool& r) { return mdea_scan_boolean(scan(), &r); }

        bool operator%(int8_t& r) { return mdea_scan_i8(scan(), &r); }
        bool operator%(int16_t& r) { return mdea_scan_i16(scan(), &r); }
        bool operator%(int32_t& r) { return mdea_scan_i32(scan(), &r); }
        bool operator%(int64_t& r) { return mdea_scan_i64(scan(), &r); }
        bool operator%(uint8_t& r) { return mdea_scan_u8(scan(), &r); }
        bool operator%(uint16_t& r) { return mdea_scan_u16(scan(), &r); }
        bool operator%(uint32_t& r) { return mdea_scan_u32(scan(), &r); }
        bool operator%(uint64_t& r) { return mdea_scan_u64(scan(), &r); }
        bool operator%(float& r) { return mdea_scan_float(scan(), &r); }
        bool operator%(double& r) { return mdea_scan_double(scan(), &r); }

        template<class E>
        requires std::is_enum_v<E>
        bool operator%(E& r)
        { return (*this) % reinterpret_cast<std::underlying_type_t<E>&>(r); }

        bool operator%(const char *& r) { return mdea_scan_string(scan(), &r); }

        bool operator%(std::string& r)
        {
                const char *t;
                if (!mdea_scan_string(scan(), &t)) return false;
                r = t;
                return true;
        }

        bool operator%(std::string_view& r)
        {
                const char *t;
                if (!mdea_scan_string(scan(), &t)) return false;
                r = t;
                return true;
        }

        Lock array() { return mdea_scan_array(scan()) ? Lock{scan()} : Lock{}; }
        Lock object() { return mdea_scan_object(scan()) ? Lock{scan()} : Lock{}; }

        bool error(const char* message) { return mdea_scan_error(scan(), message); }
        bool error(const std::string& message) { return error(message.data()); }
};

struct String_scanner : public Scanner<String_scanner> {
        friend class Scanner<String_scanner>;
 protected:
        mdea_scanner scan{nullptr};
 public:
        String_scanner(const char *s) { mdea_scanner_init_string(&scan, s, strlen(s)); }
        String_scanner(const std::string& s) : String_scanner{s.data()} {}
        String_scanner(const std::string_view& s) : String_scanner{s.data()} {}
        ~String_scanner() { mdea_scanner_free(&scan); }
};

struct File_scanner : public Scanner<File_scanner> {
        friend class Scanner<File_scanner>;
 protected:
        mdea_scanner scan{nullptr};
 public:
        File_scanner(int fd) { mdea_scanner_init_file(&scan, fd); }
        File_scanner(const char *path) { mdea_scanner_init_path(&scan, path); }
        File_scanner(const std::string& path) : File_scanner{path.data()} {}
        File_scanner(const std::filesystem::path& path) : File_scanner{path.string()} {}
        ~File_scanner() { mdea_scanner_free(&scan); }
};

template<class T> struct is_scanner : public std::integral_constant<bool, false> {};
template<> struct is_scanner<String_scanner> : public std::integral_constant<bool, true> {};
template<> struct is_scanner<File_scanner> : public std::integral_constant<bool, true> {};
template<class T> concept scanner = is_scanner<T>::value;

}; // namespace mdea

#endif // __cplusplus

#endif // mdea_scan_h
