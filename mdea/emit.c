// MDEA emitter

// Copyright (C) 2021-2024 L. Sanz <luis.sanz@gmail.com>

// This file is part of MDEA.

// MDEA is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// MDEA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with MDEA.  If not, see <https://www.gnu.org/licenses/>.

#include "mdea/emit.h"

struct mdea_emitter* mdea_emitter_init_file(struct mdea_emitter *d,
                const char *path, int flags)
{
        d->f = fopen(path, "w");
        if (d->f == NULL) {
                d->error = mdea_emitter_can_not_open;
                return NULL;
        }
        d->flags = flags;
        d->error = mdea_emitter_ok;
        d->stackp = d->stack;
        *d->stackp++ = mdea_emitter_state_root;
        return d;
}

struct mdea_emitter* mdea_emitter_init_mem(struct mdea_emitter *d,
                char **buf, size_t *len, int flags)
{
#ifdef MDEA_NO_MEMSTREAMS
        fprintf(stderr, "MDEA was compiled without memstream support\n");
        return NULL;
#else
        d->f = open_memstream(buf, len);
        if (d->f == NULL) {
                d->error = mdea_emitter_can_not_open;
                return NULL;
        }
        d->flags = flags;
        d->error = mdea_emitter_ok;
        d->stackp = d->stack;
        *d->stackp++ = mdea_emitter_state_root;
        return d;
#endif
}

void mdea_emitter_free(struct mdea_emitter* d)
{
        fclose(d->f);
}

int mdea_emitter_ready(const struct mdea_emitter *e)
{
        switch (*e->stackp) {
         case mdea_emitter_state_root:
         case mdea_emitter_state_array_first:
         case mdea_emitter_state_array:
                return 1;
         case mdea_emitter_state_object_first:
         case mdea_emitter_state_object:
                return e->next != NULL;
         default:
                return 0;
        }
}

static inline int _error(struct mdea_emitter *d, int error)
{
        d->error = error;
        return 0;
}

static inline int _newline(struct mdea_emitter *d)
{
        if (d->flags & mdea_emitter_indent) {
                if (fputc('\n', d->f) == EOF)
                        return _error(d, mdea_emitter_io_error);
                for (int i = d->stackp - d->stack; i != 0; --i)
                        if (fputs("  ", d->f) == EOF)
                                return _error(d, mdea_emitter_io_error);
        }
        return 1;
}

static inline int _value(struct mdea_emitter* d)
{
        if (d->error)
                return 0;
        if (d->stackp <= d->stack)
                return _error(d, mdea_emitter_stack_underflow);
        switch (d->stackp[-1]) {
         case mdea_emitter_state_root:
                --d->stackp;
                return 1;
         case mdea_emitter_state_array_first:
                d->stackp[-1] = mdea_emitter_state_array;
                return 1;
         case mdea_emitter_state_array:
                if (fputc(',', d->f) == EOF)
                        return _error(d, mdea_emitter_io_error);
                return _newline(d);
         case mdea_emitter_state_object_first:
                d->stackp[-1] = mdea_emitter_state_object;
                if (d->next == NULL)
                        return 0;
                fprintf(d->f, "\"%s\":", d->next);
                d->next = 0;
                return 1;
         case mdea_emitter_state_object:
                if (fputc(',', d->f) == EOF)
                        return _error(d, mdea_emitter_io_error);
                if (_newline(d) == 0)
                        return 0;
                if (d->next == NULL)
                        return 0;
                fprintf(d->f, "\"%s\":", d->next);
                d->next = 0;
                return 1;
         default:
                return _error(d, mdea_emitter_expected_key);
        }
}

int mdea_emit_next(struct mdea_emitter *e)
{
        switch (e->stackp[-1]) {
         case mdea_emitter_state_array_first:
         case mdea_emitter_state_array:
                return 1;
         default:
                return 0;
        }
}

int mdea_emit_name(struct mdea_emitter *e, const char *n)
{
        switch (e->stackp[-1]) {
         case mdea_emitter_state_object_first:
         case mdea_emitter_state_object:
                e->next = n;
                return 1;
         default:
                return 0;
        }
}

int mdea_emit_null(struct mdea_emitter *d)
{
        if (_value(d) == 0)
                return 0;
        if (fputs("null", d->f) == EOF)
                return _error(d, mdea_emitter_io_error);
        return 1;
}

int mdea_emit_boolean(struct mdea_emitter *d, bool b)
{
        if (_value(d) == 0)
                return 0;
        if (fputs(b ? "true" : "false", d->f) == EOF)
                return _error(d, mdea_emitter_io_error);
        return 1;
}

int mdea_emit_number(struct mdea_emitter *d, const char *n)
{
        if (_value(d) == 0)
                return 0;
        if (fprintf(d->f, "%s", n) < 0)
                return _error(d, mdea_emitter_io_error);
        return 1;
}

int mdea_emit_i8(struct mdea_emitter *d, int8_t n)
{
        if (_value(d) == 0)
                return 0;
        if (fprintf(d->f, "%" PRId8, n) < 0)
                return _error(d, mdea_emitter_io_error);
        return 1;
}

int mdea_emit_i16(struct mdea_emitter *d, int16_t n)
{
        if (_value(d) == 0)
                return 0;
        if (fprintf(d->f, "%" PRId16, n) < 0)
                return _error(d, mdea_emitter_io_error);
        return 1;
}

int mdea_emit_i32(struct mdea_emitter *d, int32_t n)
{
        if (_value(d) == 0)
                return 0;
        if (fprintf(d->f, "%" PRId32, n) < 0)
                return _error(d, mdea_emitter_io_error);
        return 1;
}

int mdea_emit_i64(struct mdea_emitter *d, int64_t n)
{
        if (_value(d) == 0)
                return 0;
        if (fprintf(d->f, "%" PRId64, n) < 0)
                return _error(d, mdea_emitter_io_error);
        return 1;
}

int mdea_emit_u8(struct mdea_emitter *d, uint8_t n)
{
        if (_value(d) == 0)
                return 0;
        if (fprintf(d->f, "%" PRIu8, n) < 0)
                return _error(d, mdea_emitter_io_error);
        return 1;
}

int mdea_emit_u16(struct mdea_emitter *d, uint16_t n)
{
        if (_value(d) == 0)
                return 0;
        if (fprintf(d->f, "%" PRIu16, n) < 0)
                return _error(d, mdea_emitter_io_error);
        return 1;
}

int mdea_emit_u32(struct mdea_emitter *d, uint32_t n)
{
        if (_value(d) == 0)
                return 0;
        if (fprintf(d->f, "%" PRIu32, n) < 0)
                return _error(d, mdea_emitter_io_error);
        return 1;
}

int mdea_emit_u64(struct mdea_emitter *d, uint64_t n)
{
        if (_value(d) == 0)
                return 0;
        if (fprintf(d->f, "%" PRIu64, n) < 0)
                return _error(d, mdea_emitter_io_error);
        return 1;
}

int mdea_emit_float(struct mdea_emitter *d, float n)
{
        if (_value(d) == 0)
                return 0;
        if (fprintf(d->f, "%g", n) < 0)
                return _error(d, mdea_emitter_io_error);
        return 1;
}

int mdea_emit_double(struct mdea_emitter *d, double n)
{
        if (_value(d) == 0)
                return 0;
        if (fprintf(d->f, "%g", n) < 0)
                return _error(d, mdea_emitter_io_error);
        return 1;
}

int mdea_emit_string(struct mdea_emitter *d, const char *s)
{
        if (_value(d) == 0)
                return 0;
        if (fprintf(d->f, "\"%s\"", s) < 0)
                return _error(d, mdea_emitter_io_error);
        return 1;
}

int mdea_emit_array(struct mdea_emitter *d)
{
        if (_value(d) == 0)
                return 0;
        *d->stackp++ = mdea_emitter_state_array_first;
        if (fputc('[', d->f) == EOF)
                return _error(d, mdea_emitter_io_error);
        return _newline(d);
}

int mdea_emit_object(struct mdea_emitter *d)
{
        if (_value(d) == 0)
                return 0;
        *d->stackp++ = mdea_emitter_state_object_first;
        d->next = NULL;
        if (fputc('{', d->f) == EOF)
                return _error(d, mdea_emitter_io_error);
        return _newline(d);
}

int mdea_emit_end(struct mdea_emitter* d)
{
        if (d->error)
                return 0;
        switch (d->stackp[-1]) {
         case mdea_emitter_state_array_first:
         case mdea_emitter_state_array:
                --d->stackp;
                if (_newline(d) == 0)
                        return 0;
                if (fputc(']', d->f) == EOF)
                        return _error(d, mdea_emitter_io_error);
                return 1;
         case mdea_emitter_state_object_first:
         case mdea_emitter_state_object:
                --d->stackp;
                if (_newline(d) == 0)
                        return 0;
                if (fputc('}', d->f) == EOF)
                        return _error(d, mdea_emitter_io_error);
                return 1;
         default:
                return _error(d, mdea_emitter_expected_value);
        }
}

const char* mdea_emitter_error_message(int e)
{
        switch (e) {
         case mdea_emitter_ok: return "ok";
         case mdea_emitter_can_not_open: return "can not open";
         case mdea_emitter_io_error: return "I/O error";
         case mdea_emitter_expected_key: return "expected key";
         case mdea_emitter_expected_value: return "expected value";
         case mdea_emitter_stack_underflow: return "stack underflow";
         default: return "unknown error";
        }
}

