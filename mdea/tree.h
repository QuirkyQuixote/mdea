// header for the C implementation of MDEA

// Copyright (C) 2021-2023 L. Sanz <luis.sanz@gmail.com>

// This file is part of MDEA.

// MDEA is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// MDEA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with MDEA.  If not, see <https://www.gnu.org/licenses/>.

#ifndef mdea_tree_h
#define mdea_tree_h

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

// Values are represented by a tagged index; similar to a tagged pointer, but
// instead of referencing the whole memory it points to a table in a Document.
//
// The three last bits of an index refer to the type of the data.
//
// The special value 0 is reserved for uninitialized values.

enum mdea_type {
        mdea_unset = 0,       // uninitialized
        mdea_null = 1,        // null value
        mdea_boolean = 2,        // true or false
        mdea_number = 3,      // number string in text
        mdea_string = 4,      // quoted string in text
        mdea_array = 5,       // refers to node table
        mdea_object = 6,      // refers to node table
};

struct mdea_buffer {
        char *buf;
        size_t len;
        size_t cap;
};

// The actual tree containing the parsed document:
//
// - chars contains atomic values (numbers and strings)
// - nodes contains all arrays and objects
// - root is the root of the document
//
// Every value in the tree is represented by a int32_t value whose three last
// bits are its type as defined in enum mdea_type; the rest of the bits
// describe the actual value in a way that depends on the type.
//
// - unset and null values have no further data
//
// - bool values can be either 0 (false) or 1 (true)
//
// - number and string values contain the index of the first character of
//   the strings representing the value in the text. Note that numeric values
//   are not evaluated as numbers until the user requests so.
//
// - array and object values contain the index of the first node in the node
//   table, that holds the size of the collection; for arrays the next size
//   nodes hold the values, and for objects the next 2 * size nodes hold the
//   <key, value> pairs.

struct mdea_tree {
        struct mdea_buffer chars;
        struct mdea_buffer nodes;
        int32_t root;
};

struct mdea_pair {
        int32_t k;
        int32_t v;
};

struct mdea_array {
        int32_t len;
        int32_t buf[];
};

struct mdea_object {
        int32_t len;
        struct mdea_pair buf[];
};

// Get type of reference

static inline enum mdea_type mdea_tree_type(int32_t r) { return (enum mdea_type)(r & 7); }

// Parse a tree from a JSON string

struct mdea_tree *mdea_tree_init_string(struct mdea_tree *t, const char *s, size_t n);
struct mdea_tree *mdea_tree_init_file(struct mdea_tree *t, int fd);
struct mdea_tree *mdea_tree_init_path(struct mdea_tree *t, const char *path);

// Destroy a tree

void mdea_tree_free(struct mdea_tree *t);

// Access tree atoms
static inline int
mdea_tree_bool(const struct mdea_tree *t, int32_t r)
{ return r >> 3; }

static inline const char *
mdea_tree_number(const struct mdea_tree *t, int32_t r)
{ return t->chars.buf + (r >> 3); }

static inline const char *
mdea_tree_string(const struct mdea_tree *t, int32_t r)
{ return t->chars.buf + (r >> 3); }

static inline const struct mdea_array *
mdea_tree_array(const struct mdea_tree *t, int32_t r)
{ return (struct mdea_array *)(t->nodes.buf + (r >> 3)); }

static inline const struct mdea_object *
mdea_tree_object(const struct mdea_tree *t, int32_t r)
{ return (struct mdea_object *)(t->nodes.buf + (r >> 3)); }

int32_t mdea_tree_array_at(const struct mdea_tree *t,
                const struct mdea_array *a, size_t n);
const struct mdea_pair *mdea_tree_object_at(const struct mdea_tree *t,
                const struct mdea_object *o, const char *k);

int mdea_tree_print(struct mdea_tree *t, int32_t r, FILE *f);

#ifdef __cplusplus
}; // extern "C"
#endif // __cplusplus

#ifdef __cplusplus

#endif // __cplusplus

#endif // mdea_tree_h
