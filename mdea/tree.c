// C implementation of MDEA

// Copyright (C) 2021-2023 L. Sanz <luis.sanz@gmail.com>

// This file is part of MDEA.

// MDEA is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// MDEA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with MDEA.  If not, see <https://www.gnu.org/licenses/>.


#include "mdea/tree.h"

#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

static void _push(struct mdea_buffer *b, const void *p, size_t n)
{
        size_t o = b->len;
        b->len += n;
        if (b->len >= b->cap) {
                if (b->buf) {
                        do b->cap *= 2; while (b->cap < b->len);
                        b->buf = realloc(b->buf, b->cap * sizeof(*b->buf));
                } else {
                        b->cap = 8;
                        do b->cap *= 2; while (b->cap < b->len);
                        b->buf = malloc(b->cap * sizeof(*b->buf));
                }
        }
        memcpy(b->buf + o, p, n);
}

static const char _nullc = 0;

// Additional tree functions
static inline int32_t _bool_new(struct mdea_tree *t, int b)
{
        return (b << 3) | mdea_boolean;
}

static inline int32_t _number_new(struct mdea_tree *t, const char *s, int32_t n)
{
        size_t r = t->chars.len;
        _push(&t->chars, s, n);
        _push(&t->chars, &_nullc, 1);
        return (r << 3) | mdea_number;
}

static inline int32_t _string_new(struct mdea_tree *t, const char *s, int32_t n)
{
        size_t r = t->chars.len;
        _push(&t->chars, s, n);
        _push(&t->chars, &_nullc, 1);
        return (r << 3) | mdea_string;
}

static inline int32_t _array_new(struct mdea_tree *t, const int32_t *b, int32_t n)
{
        size_t r = t->nodes.len;
        _push(&t->nodes, &n, sizeof(n));
        _push(&t->nodes, b, n * sizeof(*b));
        return (r << 3) | mdea_array;
}

static inline int32_t _object_new(struct mdea_tree *t, const struct mdea_pair *b, int32_t n)
{
        size_t r = t->nodes.len;
        _push(&t->nodes, &n, sizeof(n));
        _push(&t->nodes, b, n * sizeof(*b));
        return (r << 3) | mdea_object;
}

// Compare the keys of two pairs in the same tree

static struct mdea_tree *sort_tree;

static int mdea_comp_pairs(struct mdea_pair *l, struct mdea_pair *r)
{
        return strcmp(mdea_tree_string(sort_tree, l->k), mdea_tree_string(sort_tree, r->k));
}

// The Scanner extracts tokens from a character sequence; it is constructed
// from an iterator and sentinel pair, and every time it is called, it will
// attempt to parse and return a new token.

// The token contains a character that describes the element that was scanned:
// and is one of 'e', 'n', 'f', 't', 'i', 'r', 's', ',', ':', '[', ']', '{', '}'

// - e is end of stream
// - n is null
// - f is false
// - t is true
// - i is an integer number
// - r is a real number
// - s is a string
// - other characters return themselves

// The token also contains two iterators to the beginning and end of the
// subrange of characters that were recognized.

// Also provides an error() function that will print an error message alongside
// location data for the last scanned token.

struct mdea_location {
        const char *first;
        const char *last;
};

struct mdea_parser {
        const char *buf;                // full buffer
        const char *end;                // end of full text
        const char *tok;                // start of token
        const char *pos;                // end of token and next character
        int tok_type;                   // type of the current token
        struct mdea_tree *tree;         // the tree being built
        struct mdea_buffer stack;       // to build arrays and objects
};

static struct mdea_location mdea_line(struct mdea_parser *p)
{
        struct mdea_location r = { p->tok, p->pos };
        while (r.first != p->buf && r.first[-1] != '\n')
                --r.first;
        while (r.last != p->end && *r.last != '\n')
                ++r.last;
        if (r.last[-1] == '\n')
                --r.last;
        return r;
}

static int mdea_lineno(struct mdea_parser *p)
{
        int n = 1;
        for (const char* i = p->buf; i != p->tok; ++i)
                if (*i == '\n') ++n;
        return n;
}

static const char *_tok_name(int tok_type)
{
        switch (tok_type) {
         case 'e': return "end of file";
         case 'n': return "null";
         case 'f': return "false";
         case 't': return "true";
         case 'i': return "integer";
         case 'r': return "float";
         case 's': return "string";
         case ',': return "','";
         case ':': return "':'";
         case '[': return "'['";
         case ']': return "']'";
         case '{': return "'{'";
         case '}': return "'}'";
         default: return "unrecognized token";
        }
}

static int _error(struct mdea_parser *p, const char* m)
{
        static const char ws[] =
                "                                        "
                "                                        "
                "                                        "
                "                                        ";
        static const char mk[] =
                "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
                "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
                "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
                "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^";
        struct mdea_location line = mdea_line(p);
        int lineno = mdea_lineno(p);
        int charno = p->tok - line.first;
        int llen = line.last - line.first;
        int tlen = p->pos - p->tok;
        fflush(stdout);
        fprintf(stderr, "%d:%d:%s before %s\n", lineno, charno, m, _tok_name(p->tok_type));
        fprintf(stderr, "%5d | %.*s\n", lineno, llen, line.first);
        fprintf(stderr, "      | %.*s%.*s\n", charno, ws, (tlen ? tlen : 1), mk);
        return -1;
}

static int mdea_scan_tag(struct mdea_parser *p)
{
        static const signed char group[256] = {
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 1, 0, 0, 0, 2, 3, 0, 0, 0, 0, 0, 4, 0, 5, 0,
                0, 0, 6, 7, 8, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        };

        static const signed char trans[14][10] = {
                //     a   e   f   l   n   r   s   t   u
                { -1, -1, -1,  5, -1,  1, -1, -1, 10, -1 },
                { -1, -1, -1, -1, -1, -1, -1, -1, -1,  2 },
                { -1, -1, -1, -1,  3, -1, -1, -1, -1, -1 },
                { -1, -1, -1, -1,  4, -1, -1, -1, -1, -1 },
                { -2, -2, -2, -2, -2, -2, -2, -2, -2, -2 },
                { -1,  6, -1, -1, -1, -1, -1, -1, -1, -1 },
                { -1, -1, -1, -1,  7, -1, -1, -1, -1, -1 },
                { -1, -1, -1, -1, -1, -1, -1,  8, -1, -1 },
                { -1, -1,  9, -1, -1, -1, -1, -1, -1, -1 },
                { -3, -3, -3, -3, -3, -3, -3, -3, -3, -3 },
                { -1, -1, -1, -1, -1, -1, 11, -1, -1, -1 },
                { -1, -1, -1, -1, -1, -1, -1, -1, -1, 12 },
                { -1, -1, 13, -1, -1, -1, -1, -1, -1, -1 },
                { -4, -4, -4, -4, -4, -4, -4, -4, -4, -4 },
        };

        p->tok = p->pos;
        for (int state = 0; p->pos != p->end; ++p->pos) {
                switch ((state = trans[state][group[(int)*p->pos]])) {
                 case -1: return _error(p, "expected tag");
                 case -2: p->tok_type = 'n'; return 0;
                 case -3: p->tok_type = 'f'; return 0;
                 case -4: p->tok_type = 't'; return 0;
                }
        }
        return _error(p, "unexpected end of text");
}

static int mdea_scan_number(struct mdea_parser *p)
{
        static const signed char group[256] = {
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 0, 5, 3, 0,
                1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        };

        static const signed char trans[7][7] = {
                //     0  1-9  .  e,E  -   +
                { -1,  2,  3, -1, -1,  1, -1 },
                { -1,  2,  3, -1, -1, -1, -1 }, // starts with -
                { -2, -2, -2,  4,  5, -2, -2 }, // found 0
                { -2,  3,  3,  4,  5, -2, -2 }, // found 1-9
                { -3,  4,  4, -3,  5, -3, -3 }, // found .
                { -1,  6,  6, -1, -1,  6,  6 }, // found e,E
                { -3,  6,  6, -3, -3, -3, -3 }, // more exponent
        };

        p->tok = p->pos;
        for (int state = 0; p->pos != p->end; ++p->pos) {
                switch ((state = trans[state][group[(int)*p->pos]])) {
                 case -1: return _error(p, "expected number");
                 case -2: p->tok_type = 'i'; return 0;
                 case -3: p->tok_type = 'r'; return 0;
                }
        }
        return _error(p, "unexpected end of text");
}

static int mdea_scan_string(struct mdea_parser *p)
{
        static const signed char A = 10, B = 11;

        static const signed char group[256] = {
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 4,
                6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 1, 1, 1, 1, 1, 1,
                1, 6, 6, 6, 6, 6, 6, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 1, 1, 1,
                1, 6, 7, 6, 6, 6, 7, 1, 1, 1, 1, 1, 1, 1, 4, 1,
                1, 1, 4, 1, 4, 5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,
                8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
                8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
                8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
                8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
                9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
                9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
                A, A, A, A, A, A, A, A, A, A, A, A, A, A, A, A,
                B, B, B, B, B, B, B, B, 0, 0, 0, 0, 0, 0, 0, 0,
        };

        static const signed char trans[11][12] = {
                //    chr  "   \  esc  u  hex b,f extended unicode
                { -1, -1,  1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
                { -1,  1,  7,  2,  1,  1,  1,  1, -1,  A,  9,  8 },
                { -1, -1,  1,  1,  1,  3, -1,  1, -1, -1, -1, -1 },
                { -1, -1, -1, -1, -1, -1,  4,  4, -1, -1, -1, -1 },
                { -1, -1, -1, -1, -1, -1,  5,  5, -1, -1, -1, -1 },
                { -1, -1, -1, -1, -1, -1,  6,  6, -1, -1, -1, -1 },
                { -1, -1, -1, -1, -1, -1,  1,  1, -1, -1, -1, -1 },
                { -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2 },
                { -1, -1, -1, -1, -1, -1, -1, -1,  9, -1, -1, -1 },
                { -1, -1, -1, -1, -1, -1, -1, -1,  A, -1, -1, -1 },
                { -1, -1, -1, -1, -1, -1, -1, -1,  1, -1, -1, -1 },
        };

        p->tok = p->pos;
        for (int state = 0; p->pos != p->end; ++p->pos) {
                switch ((state = trans[state][group[(int)*p->pos]])) {
                 case -1: return _error(p, "expected string");
                 case -2: p->tok_type = 's'; return 0;
                }
        }
        return _error(p, "unexpected end of text");
}

static int mdea_scan_symbol(struct mdea_parser *p)
{
        p->tok = p->pos++;
        p->tok_type = *p->tok;
        return 0;
}

static int mdea_scan(struct mdea_parser *p)
{
        static const signed char group[256] = {
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, 0, 5, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                5, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 2, 4, 2, 0, 0,
                2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0, 4, 0, 0,
                0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 3, 0,
                0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 4, 0, 4, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        };

        for (; p->pos != p->end; ++p->pos) {
                switch (group[(int)*p->pos]) {
                 case 1: return mdea_scan_string(p);
                 case 2: return mdea_scan_number(p);
                 case 3: return mdea_scan_tag(p);
                 case 4: return mdea_scan_symbol(p);
                 case 5: break; // skip whitespace
                 default: return _error(p, "expected token");
                }
        }
        p->tok = p->pos;
        p->tok_type = 'e';
        return 0;
}

// The Parser generates a full document from any sequence of characters that
// forms valid JSON data.

static int32_t _parse_null(struct mdea_parser *p);
static int32_t _parse_false(struct mdea_parser *p);
static int32_t _parse_true(struct mdea_parser *p);
static int32_t _parse_number(struct mdea_parser *p);
static int32_t _parse_array(struct mdea_parser *p);
static int32_t _parse_object(struct mdea_parser *p);
static int32_t _parse_value(struct mdea_parser *p);

static int32_t _parse_null(struct mdea_parser *p) { return mdea_null; }
static int32_t _parse_false(struct mdea_parser *p) { return _bool_new(p->tree, 0); }
static int32_t _parse_true(struct mdea_parser *p) { return _bool_new(p->tree, 1); }

static int32_t _parse_number(struct mdea_parser *p)
{
        return _number_new(p->tree, p->tok, p->pos - p->tok);
}

static int32_t _parse_string(struct mdea_parser *p)
{
        return _string_new(p->tree, p->tok + 1, p->pos - p->tok - 2);
}

static int32_t _parse_array(struct mdea_parser *p)
{
        if (mdea_scan(p) < 0)
                return -1;
        if (p->tok_type == ']')
                return _array_new(p->tree, NULL, 0);
        size_t off = p->stack.len;
        for (;;) {
                int32_t elem;
                if ((elem = _parse_value(p)) < 0)
                        return -1;
                _push(&p->stack, &elem, sizeof(elem));
                if (mdea_scan(p) < 0)
                        return -1;
                if (p->tok_type == ']')
                        break;
                if (p->tok_type != ',')
                        return _error(p, "expected ',' or ']'");
                if (mdea_scan(p) < 0)
                        return -1;
        }
        int32_t *i = (void *)(p->stack.buf + off);
        size_t n = (p->stack.len - off) / sizeof(*i);
        p->stack.len = off;
        return _array_new(p->tree, i, n);
}

static int32_t _parse_object(struct mdea_parser *p)
{
        if (mdea_scan(p) < 0)
                return -1;
        if (p->tok_type == '}')
                return _object_new(p->tree, NULL, 0);
        size_t off = p->stack.len;
        for (;;) {
                struct mdea_pair elem;
                if (p->tok_type != 's')
                        return _error(p, "expected key");
                if ((elem.k = _parse_string(p)) < 0)
                        return -1;
                if (mdea_scan(p) < 0)
                        return -1;
                if (p->tok_type != ':')
                        return _error(p, "expected ':'");
                if (mdea_scan(p) < 0)
                        return -1;
                if ((elem.v = _parse_value(p)) < 0)
                        return -1;
                _push(&p->stack, &elem, sizeof(elem));
                if (mdea_scan(p) < 0)
                        return -1;
                if (p->tok_type == '}')
                        break;
                if (p->tok_type != ',')
                        return _error(p, "expected ',' or '}'");
                if (mdea_scan(p) < 0)
                        return -1;
        }
        struct mdea_pair *i = (void *)(p->stack.buf + off);
        size_t n = (p->stack.len - off) / sizeof(*i);
        p->stack.len = off;
        sort_tree = p->tree;
        qsort(i, n, sizeof(*i), (void *)mdea_comp_pairs);
        return _object_new(p->tree, i, n);
}

static int32_t _parse_value(struct mdea_parser *p)
{
        switch (p->tok_type) {
         case 'n': return _parse_null(p);
         case 'f': return _parse_false(p);
         case 't': return _parse_true(p);
         case 'i': return _parse_number(p);
         case 'r': return _parse_number(p);
         case 's': return _parse_string(p);
         case '[': return _parse_array(p);
         case '{': return _parse_object(p);
         default: return _error(p, "expected value");
        }
}

static int _parse(struct mdea_parser *p)
{
        if (mdea_scan(p) < 0)
                return -1;
        if ((p->tree->root = _parse_value(p)) < 0)
                return -1;
        if (mdea_scan(p) < 0)
                return -1;
        if (p->tok_type != 'e')
                return _error(p, "expected end");
        return 0;
}

struct mdea_tree *mdea_tree_init_string(struct mdea_tree *t, const char *s, size_t n)
{
        memset(t, 0, sizeof(*t));
        struct mdea_parser p = {
                .buf = s,
                .end = s + n,
                .pos = s,
                .tree = t,
        };
        int err = _parse(&p);
        free(p.stack.buf);
        return err ? NULL : t;
}

struct mdea_tree *mdea_tree_init_file(struct mdea_tree *t, int fd)
{
        size_t len = 16;
        size_t off = 0;
        char *buf = malloc(len);
        for (;;) {
                if (buf == NULL) {
                        perror("alloc");
                        abort();
                }
                ssize_t ret = read(fd, buf + off, len - off);
                if (ret == -1) {
                        perror("read");
                        free(buf);
                        return NULL;
                }
                if (ret == 0)
                        break;
                off += ret;
                if (off == len) {
                        len *= 2;
                        buf = realloc(buf, len);
                }
        }
        buf[off] = 0;
        t = mdea_tree_init_string(t, buf, off);
        free(buf);
        return t;
}

struct mdea_tree *mdea_tree_init_path(struct mdea_tree *t, const char *path)
{
        int fd = open(path, O_RDONLY);
        if (fd != -1) {
                t = mdea_tree_init_file(t, fd);
                close(fd);
                return t;
        }
        perror(path);
        return NULL;
}

void mdea_tree_free(struct mdea_tree *t)
{
        free(t->chars.buf);
        free(t->nodes.buf);
}

int32_t mdea_tree_array_at(const struct mdea_tree *t,
                const struct mdea_array *a, size_t n)
{
        if (n < a->len)
                return a->buf[n];
        return 0;
}

const struct mdea_pair *mdea_tree_object_at(const struct mdea_tree *t,
                const struct mdea_object *o, const char *k)
{
        const struct mdea_pair *i = o->buf;
        size_t n = o->len;
        while (n != 0) {
                size_t s = n / 2;
                const struct mdea_pair *j = i + s;
                int d = strcmp(mdea_tree_string(t, j->k), k);
                if (d < 0) {
                        i = j + 1;
                        n -= s + 1;
                } else if (d > 0) {
                        n = s;
                } else {
                        return j;
                }
        }
        return 0;
}

int mdea_tree_print(struct mdea_tree *t, int32_t r, FILE *f)
{
        switch (mdea_tree_type(r)) {
         case mdea_unset:
                return fputc('!', f);
         case mdea_null:
                return fputs("null", f);
         case mdea_boolean:
                return fputs(mdea_tree_bool(t, r) ? "true" : "false", f);
         case mdea_number:
                return fputs(mdea_tree_number(t, r), f);
         case mdea_string:
                return fputc('"', f)
                        + fputs(mdea_tree_string(t, r), f)
                        + fputc('"', f);
         case mdea_array:
                {
                        fputc('[', f);
                        const struct mdea_array *a = mdea_tree_array(t, r);
                        if (a->len) {
                                mdea_tree_print(t, *a->buf, f);
                                const int32_t *i = a->buf + 1;
                                const int32_t *s = a->buf + a->len;
                                while (i != s) {
                                        fputc(',', f);
                                        mdea_tree_print(t, *i, f);
                                        ++i;
                                }
                        }
                        return fputc(']', f);
                }
         case mdea_object:
                {
                        fputc('{', f);
                        const struct mdea_object *o = mdea_tree_object(t, r);
                        if (o->len) {
                                mdea_tree_print(t, o->buf->k, f);
                                fputc(':', f);
                                mdea_tree_print(t, o->buf->v, f);
                                const struct mdea_pair *i = o->buf + 1;
                                const struct mdea_pair *s = o->buf + o->len;
                                while (i != s) {
                                        fputc(',', f);
                                        mdea_tree_print(t, i->k, f);
                                        fputc(':', f);
                                        mdea_tree_print(t, i->v, f);
                                        ++i;
                                }
                        }
                        return fputc('}', f);
                }
        }
        abort(); // unreachable
}

