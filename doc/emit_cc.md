
# mdea/emit.h

The "mdea/emit.h" provides an emitter that writes C++ data to JSON.

```cpp
template<class Derived> mdea::Emitter;
template<class Derived> mdea::Emitter::Lock;
mdea::File_emitter : public mdea::Emitter<File_emitter>;
mdea::String_emitter : public mdea::Emitter<String_emitter>;
```

## Emitter

Base class of all emitters through CRTP.

```cpp
Emitter() [protected];
Emitter(const Emitter&) = delete;
Emitter(Emitter&&) = delete;
```

The default constructor is protected so only classes inheriting from it can
actually instance this, and the copy and move constructors are deleted.

```cpp
Derived& next();
Derived& name(const char *n);
Lock array();
Lock object();
constexpr operator bool() const;
```

Navigate document

A document hierarchy is navigated with a stack. the element being edited is the
one at the top of the stack, and its parents going back to the root are below
it in the stack.

The `array()` and `object()` functions will push a new element to the stack of
that particular type; they return a `Lock` that is used to control the lifetime
of that object at the top of the stack: when the `Lock` is destroyed the object
is popped from the stack.

The `next()` function will only work when the element at the top of the stack
is an array and it advances to its next element.

The `name()` function will only work when the element at the top of the stack
is an object and it moves to the element named `n`.

Casting the `Emitter` to `bool` returns `true` if the emitter is ready to write
a value, `false` otherwise.

```cpp
bool operator%(std::nullopt_t);
bool operator%(bool b);
bool operator%(int8_t n);
bool operator%(int16_t n);
bool operator%(int32_t n);
bool operator%(int64_t n);
bool operator%(uint8_t n);
bool operator%(uint16_t n);
bool operator%(uint32_t n);
bool operator%(uint64_t n);
bool operator%(float n);
bool operator%(double n);
template<class E> requires std::is_enum_v<E> bool operator%(E n);
bool operator%(const char *s);
bool operator%(const std::string& s);
bool operator%(const std::string_view& s);
```

Write values

These functions write the given value to the element the emitter is pointing
at; they will fail and return `false` if no element is pointed, otherwise they
return `true`.

## File_emitter

```cpp
File_emitter(const char* path, int flags = mdea_emitter_indent);
File_emitter(const std::string& path, int flags = mdea_emitter_indent);
File_emitter(const std::filesystem::path& path, int flags = mdea_emitter_indent);
```

This specialization of `Emitter` writes to the file pointed at by `path`.

## String_emitter

```
String_emitter(int flags = mdea_emitter_indent);
std::string_view string();
```

This specialization of `Emitter` writes to an string. A view of the string can
be queried at any type with the `string()` function. Note that the string will
be destroyed at the same time as the emitter.

## Concepts

```cpp
template<class T> struct is_emitter;
template<class T> concept emitter = is_emitter<T>::value;
```

