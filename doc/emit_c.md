
# mdea/emit.h

The "mdea/emit.h" provides an emitter that writes C data to JSON.

## Types

```c
enum mdea_emitter_flags {
        mdea_emitter_indent = 1 << 0,
};

enum mdea_emitter_state {
        mdea_emitter_state_root,
        mdea_emitter_state_array_first,
        mdea_emitter_state_array,
        mdea_emitter_state_object_first,
        mdea_emitter_state_object,
};

enum mdea_emitter_error {
        mdea_emitter_ok,
        mdea_emitter_can_not_open,
        mdea_emitter_io_error,
        mdea_emitter_expected_key,
        mdea_emitter_expected_value,
        mdea_emitter_stack_underflow,
};

struct mdea_emitter {
        FILE *f;
        int flags;
        int error;
        enum mdea_emitter_state stack[64];
        enum mdea_emitter_state *stackp;
        const char *next;
};
```

## Functions

```c
struct mdea_emitter* mdea_emitter_init_file(struct mdea_emitter *d,
                const char *path, int flags);
struct mdea_emitter* mdea_emitter_init_mem(struct mdea_emitter *d,
                char **buf, size_t *len, int flags);
```

Construct an emitter using the given `flags` and additional data that depends
on the function:

`mdea_emitter_init_file()` initializes an emitter that will write to the file
at `path`.

`mdea_emitter_init_mem()` initializes an emitter that will write to a
dynamically allocated block of memory, and uses the locations pointed by `buf`
and `len` to report back the pointer to the generated string and its length.
The pointers are only guaranteed to be updated after `mdea_emitter_free()` is
called, and the user should call `free()` on the resulting memory.

```c
void mdea_emitter_free(struct mdea_emitter* d);
```

Destroy emitter

```c
int mdea_emit_next(struct mdea_emitter *e);
int mdea_emit_name(struct mdea_emitter *e, const char *n);
int mdea_emit_array(struct mdea_emitter* d);
int mdea_emit_object(struct mdea_emitter *d);
int mdea_emit_end(struct mdea_emitter* d);
int mdea_emitter_ready(const struct mdea_emitter *e);
```

Navigate document

A document hierarchy is navigated with a stack. the element being edited is the
one at the top of the stack, and its parents going back to the root are below
it in the stack.

The `mdea_emit_array()` and `mdea_emit_object()` functions will push a new
element to the stack of that particular type.

The `mdea_emit_end()` function will pop the element from the stack

The `mdea_emit_next()` function will only work when the element at the top of
the stack is an array and it advances to its next element.

The `mdea_emit_name()` function will only work when the element at the top of
the stack is an object and it moves to the element named `n`.

The `mdea_emitter_ready()` function checks if the emitter is ready to have an
element written.

```c
int mdea_emit_null(struct mdea_emitter* d);
int mdea_emit_boolean(struct mdea_emitter* d, bool b);
int mdea_emit_number(struct mdea_emitter* d, const char *n);
int mdea_emit_i8(struct mdea_emitter* d, int8_t n);
int mdea_emit_i16(struct mdea_emitter* d, int16_t n);
int mdea_emit_i32(struct mdea_emitter* d, int32_t n);
int mdea_emit_i64(struct mdea_emitter* d, int64_t n);
int mdea_emit_u8(struct mdea_emitter* d, uint8_t n);
int mdea_emit_u16(struct mdea_emitter* d, uint16_t n);
int mdea_emit_u32(struct mdea_emitter* d, uint32_t n);
int mdea_emit_u64(struct mdea_emitter* d, uint64_t n);
int mdea_emit_float(struct mdea_emitter* d, float n);
int mdea_emit_double(struct mdea_emitter* d, double n);
int mdea_emit_string(struct mdea_emitter* d, const char *s);
```

Write values

These functions write the given value to the element the emitter is pointing
at; they will fail and return `0` if no element is pointed, otherwise they
return `1`.

