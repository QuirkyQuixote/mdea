
# mdea/scan.h

The "mdea/scan.h" provides a scanner that reads C data from JSON.

## Types

```c
struct mdea_scanner_stack_array {
        int8_t type;
        const struct mdea_array *arr;
        uint32_t next;
};

struct mdea_scanner_stack_object {
        int8_t type;
        const struct mdea_object *obj;
};

union mdea_scanner_stack {
        int8_t type;
        struct mdea_scanner_stack_array arr;
        struct mdea_scanner_stack_object obj;
};

struct mdea_scanner {
        struct mdea_tree tree;
        union mdea_scanner_stack stack[64];
        union mdea_scanner_stack *stackp;
        int32_t val;
};
```

## Functions

```c
struct mdea_scanner *mdea_scanner_init_string(struct mdea_scanner *s, const char *p, size_t n);
struct mdea_scanner *mdea_scanner_init_file(struct mdea_scanner *s, int fd);
struct mdea_scanner *mdea_scanner_init_path(struct mdea_scanner *s, const char *path);
```
Construct scanner.

`mdea_scanner_init_string()` will read the text from the string at the memory
pointed by `p` of `n` bytes long; it does not have to be null-terminated.

`mdea_scanner_init_file()` will read the text from the file whose descriptor is
`fd`.

`mdea_scanner_init_path()` will read the text from the file at `path`.


```c
void mdea_scanner_free(struct mdea_scanner *s);
```

Destroy scanner.

```c
int mdea_scan_next(struct mdea_scanner *s);
int mdea_scan_name(struct mdea_scanner *s, const char *n);
int mdea_scan_array(struct mdea_scanner *s);
int mdea_scan_object(struct mdea_scanner *s);
int mdea_scan_end(struct mdea_scanner *s);
int mdea_scanner_ready(const struct mdea_scanner *s);
```

Navigate document

A document hierarchy is navigated with a stack. the element being edited is the
one at the top of the stack, and its parents going back to the root are below
it in the stack.

The `mdea_scan_array()` and `mdea_scan_object()` functions will push a new
element to the stack of that particular type; they will fail and return `0` if
the current value in the document is not of the right type, otherwise they
return `1`.

The `mdea_scan_end()` function will pop the element from the stack

The `mdea_scan_next()` function will only work when the element at the top of
the stack is an array and it advances to its next element. It returns `1` if
there's such an element or `0` otherwise.

The `mdea_scan_name()` function will only work when the element at the top of
the stack is an object and it moves to the element named `n`. It returns `1` if
there's such an element or `0` otherwise and if so, an message is printed to
`stderr` notifying the user.

The `mdea_scanner_ready()` function checks if the scanner is ready to have an
element read.

```c
int mdea_scan_null(struct mdea_scanner *s);
int mdea_scan_boolean(struct mdea_scanner *s, bool *r);
int mdea_scan_number(struct mdea_scanner *s, const char **r);
int mdea_scan_i8(struct mdea_scanner *s, int8_t *r);
int mdea_scan_i16(struct mdea_scanner *s, int16_t *r);
int mdea_scan_i32(struct mdea_scanner *s, int32_t *r);
int mdea_scan_i64(struct mdea_scanner *s, int64_t *r);
int mdea_scan_u8(struct mdea_scanner *s, uint8_t *r);
int mdea_scan_u16(struct mdea_scanner *s, uint16_t *r);
int mdea_scan_u32(struct mdea_scanner *s, uint32_t *r);
int mdea_scan_u64(struct mdea_scanner *s, uint64_t *r);
int mdea_scan_float(struct mdea_scanner *s, float *r);
int mdea_scan_double(struct mdea_scanner *s, double *r);
int mdea_scan_string(struct mdea_scanner *s, const char **r);
```

Read values

These functions read the given value to the element the scanner is pointing
at; they will fail and return `0` if no element is pointed, otherwise they
return `1`.


