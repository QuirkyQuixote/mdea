
# mdea/scan.h

The "mdea/scan.h" provides a scanner that reads C++ data from JSON.

```cpp
template<class Derived> class Scanner;
template<class Derived> class Scanner::Lock;
class File_scanner : public Scanner<File_scanner>;
class String_scanner : public Scanner<String_scanner>;
```

## Scanner

```cpp
Scanner() [protected];
Scanner(const Scanner&) = delete;
Scanner(Scanner&&) = delete;
```

The default constructor is protected so only classes inheriting from it can
actually instance this, and the copy and move constructors are deleted.

```cpp
Derived& next();
Derived& name(const char* n);
Lock array();
Lock object();
constexpr operator bool() const
```

Navigate document

A document hierarchy is navigated with a stack. the element being edited is the
one at the top of the stack, and its parents going back to the root are below
it in the stack.

The `array()` and `object()` functions will push a new element to the stack of
that particular type; they return a `Lock` that is used to control the lifetime
of that object at the top of the stack: when the `Lock` is destroyed the object
is popped from the stack. If the internal cursor does not point to a value of
the right type, the `Lock` object will be empty and evaluate to `false`.

The `next()` function will only work when the element at the top of
the stack is an array and it advances to its next element. It returns `1` if
there's such an element or `0` otherwise.

The `name()` function will only work when the element at the top of
the stack is an object and it moves to the element named `n`. It returns `1` if
there's such an element or `0` otherwise and if so, an message is printed to
`stderr` notifying the user.

Casting the `Scanner` to `bool` returns `true` if the scanner is ready to read
a value, `false` otherwise.

```cpp
bool operator%(const std::nullopt_t&);
bool operator%(bool& r);
bool operator%(int8_t& r);
bool operator%(int16_t& r);
bool operator%(int32_t& r);
bool operator%(int64_t& r);
bool operator%(uint8_t& r);
bool operator%(uint16_t& r);
bool operator%(uint32_t& r);
bool operator%(uint64_t& r);
bool operator%(float& r);
bool operator%(double& r);
template<class E> requires std::is_enum_v<E> bool operator%(E& r);
bool operator%(const char *& r);
bool operator%(std::string& r);
bool operator%(std::string_view& r);
```

Read values

These functions read the given value to the element the emitter is pointing
at; they will fail and return `false` if no element is pointed, otherwise they
return `true`.

## String_scanner

```cpp
String_scanner(const char *s);
String_scanner(const std::string& s);
String_scanner(const std::string_view& s);
```

This specialization of `Scanner` reads from the string `s`.

## File_scanner

```cpp
File_scanner(int fd);
File_scanner(const char *path);
File_scanner(const std::string& path);
File_scanner(const std::filesystem::path& path);
```

This specialization of `Scanner` reads from a file; you can create it with
either a valid file `path` or a file descriptor.

## Concepts

```cpp
template<class T> struct is_scanner;
template<class T> concept scanner = is_scanner<T>::value;
```
