// Test for the C MDEA emitter

// Copyright (C) 2021-2024 L. Sanz <luis.sanz@gmail.com>

// This file is part of MDEA.

// MDEA is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// MDEA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with MDEA.  If not, see <https://www.gnu.org/licenses/>.

#include <stdlib.h>

#include "mdea/emit.h"

int _parse(struct mdea_emitter *s)
{
        bool boolean = 1;
        double number = 3.141592;
        const char *string = "Hello, World!";
        double arr[3] = {1,2,3};
        double obj[3] = {4,5,6};

        if (mdea_emit_object(s)) {
                if (mdea_emit_name(s, "null")) mdea_emit_null(s);
                if (mdea_emit_name(s, "boolean")) mdea_emit_boolean(s, boolean);
                if (mdea_emit_name(s, "number")) mdea_emit_double(s, number);
                if (mdea_emit_name(s, "string")) mdea_emit_string(s, string);
                if (mdea_emit_name(s, "array") && mdea_emit_array(s)) {
                        if (mdea_emit_next(s)) mdea_emit_double(s, arr[0]);
                        if (mdea_emit_next(s)) mdea_emit_double(s, arr[1]);
                        if (mdea_emit_next(s)) mdea_emit_double(s, arr[2]);
                        mdea_emit_end(s);
                }
                if (mdea_emit_name(s, "object") && mdea_emit_object(s)) {
                        if (mdea_emit_name(s, "foo")) mdea_emit_double(s, obj[0]);
                        if (mdea_emit_name(s, "bar")) mdea_emit_double(s, obj[1]);
                        if (mdea_emit_name(s, "baz")) mdea_emit_double(s, obj[2]);
                        mdea_emit_end(s);
                }
                mdea_emit_end(s);
        }

        return 0;
}

int main(int argc, char *argv[])
{
        char *buf = NULL;
        size_t len = 0;
        struct mdea_emitter emit;
        if (mdea_emitter_init_mem(&emit, &buf, &len, mdea_emitter_indent))
                _parse(&emit);
        mdea_emitter_free(&emit);
        fprintf(stdout, "%s\n", buf);
        free(buf);
        return 0;
}


