// Test for the C++ MDEA scanner

// Copyright (C) 2021-2024 L. Sanz <luis.sanz@gmail.com>

// This file is part of MDEA.

// MDEA is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// MDEA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with MDEA.  If not, see <https://www.gnu.org/licenses/>.

#include "mdea/scan.h"

static const char json[] = "{"
"  \"null\":null,\n"
"  \"boolean\":true,\n"
"  \"number\":3.141592,\n"
"  \"string\":\"Hello, World!\",\n"
"  \"array\":[1,2,3],\n"
"  \"object\":{\"foo\":4,\"bar\":5,\"baz\":6}\n"
"}";

struct Data {
        bool boolean{0};
        double number{0};
        const char *string{""};
        double array[3] = {0};
        double object[3] = {0};
};

bool operator%(mdea::scanner auto& s, Data& d)
{
        if (auto _ = s.object()) {
                s.name("null") % std::nullopt;
                s.name("boolean") % d.boolean;
                s.name("number") % d.number;
                s.name("string") % d.string;
                if (auto _ = s.name("array").array()) {
                         s.next() % d.array[0];
                         s.next() % d.array[1];
                         s.next() % d.array[2];
                }
                if (auto _ = s.name("object").object()) {
                         s.name("foo") % d.object[0];
                         s.name("bar") % d.object[1];
                         s.name("baz") % d.object[2];
                }
                return true;
        }
        return false;
}

int main(int argc, char *argv[])
{
        Data data;

        mdea::String_scanner scan{json};
        scan % data;

        fprintf(stdout, "boolean=%d\n", data.boolean);
        fprintf(stdout, "number=%g\n", data.number);
        fprintf(stdout, "string=%s\n", data.string);
        fprintf(stdout, "arr=[%g,%g,%g]\n",
                        data.array[0], data.array[1], data.array[2]);
        fprintf(stdout, "obj={\"foo\":%g,\"bar\":%g,\"baz\":%g}\n",
                        data.object[0], data.object[1], data.object[2]);
        return 0;
}

