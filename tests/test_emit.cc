// Test for the C++ MDEA emitter

// Copyright (C) 2021-2024 L. Sanz <luis.sanz@gmail.com>

// This file is part of MDEA.

// MDEA is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// MDEA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with MDEA.  If not, see <https://www.gnu.org/licenses/>.

#include "mdea/emit.h"

struct Data {
        bool boolean{true};
        double number{3.141592};
        const char *string{"Hello, World!"};
        double array[3] = {1,2,3};
        double object[3] = {4,5,6};
};

bool operator%(mdea::emitter auto& s, const Data& d)
{
        if (auto _ = s.object()) {
                s.name("null") % std::nullopt;
                s.name("boolean") % d.boolean;
                s.name("number") % d.number;
                s.name("string") % d.string;
                if (auto _ = s.name("array").array()) {
                         s.next() % d.array[0];
                         s.next() % d.array[1];
                         s.next() % d.array[2];
                }
                if (auto _ = s.name("object").object()) {
                         s.name("foo") % d.object[0];
                         s.name("bar") % d.object[1];
                         s.name("baz") % d.object[2];
                }
                return true;
        }
        return false;
}

int main(int argc, char *argv[])
{
        Data data;

        mdea::String_emitter emit{};
        emit % data;

        fprintf(stdout, "%s\n", emit.string().data());
        return 0;
}


