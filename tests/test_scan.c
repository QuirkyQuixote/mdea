// Test for the C MDEA scanner

// Copyright (C) 2021-2024 L. Sanz <luis.sanz@gmail.com>

// This file is part of MDEA.

// MDEA is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// MDEA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with MDEA.  If not, see <https://www.gnu.org/licenses/>.

#include "mdea/scan.h"


static const char json[] = "{"
"  \"null\":null,\n"
"  \"boolean\":true,\n"
"  \"number\":3.141592,\n"
"  \"string\":\"Hello, World!\",\n"
"  \"array\":[1,2,3],\n"
"  \"object\":{\"foo\":4,\"bar\":5,\"baz\":6}\n"
"}\n";

int _parse(struct mdea_scanner *s)
{
        bool boolean = 0;
        double number = 0;
        const char *string = "";
        double arr[3] = {0};
        double obj[3] = {0};

        if (mdea_scan_object(s)) {
                if (mdea_scan_name(s, "null")) mdea_scan_null(s);
                if (mdea_scan_name(s, "boolean")) mdea_scan_boolean(s, &boolean);
                if (mdea_scan_name(s, "number")) mdea_scan_double(s, &number);
                if (mdea_scan_name(s, "string")) mdea_scan_string(s, &string);
                if (mdea_scan_name(s, "array") && mdea_scan_array(s)) {
                        if (mdea_scan_next(s)) mdea_scan_double(s, arr + 0);
                        if (mdea_scan_next(s)) mdea_scan_double(s, arr + 1);
                        if (mdea_scan_next(s)) mdea_scan_double(s, arr + 2);
                        mdea_scan_end(s);
                }
                if (mdea_scan_name(s, "object") && mdea_scan_object(s)) {
                        if (mdea_scan_name(s, "foo")) mdea_scan_double(s, obj + 0);
                        if (mdea_scan_name(s, "bar")) mdea_scan_double(s, obj + 1);
                        if (mdea_scan_name(s, "baz")) mdea_scan_double(s, obj + 2);
                        mdea_scan_end(s);
                }
                mdea_scan_end(s);
        }

        fprintf(stdout, "boolean=%d\n", boolean);
        fprintf(stdout, "number=%g\n", number);
        fprintf(stdout, "string=%s\n", string);
        fprintf(stdout, "arr=[%g,%g,%g]\n", arr[0], arr[1], arr[2]);
        fprintf(stdout, "obj={\"foo\":%g,\"bar\":%g,\"baz\":%g}\n", obj[0], obj[1], obj[2]);
        return 0;
}

int main(int argc, char *argv[])
{
        struct mdea_scanner scan;
        if (mdea_scanner_init_string(&scan, json, strlen(json)))
                _parse(&scan);
        mdea_scanner_free(&scan);
        return 0;
}

